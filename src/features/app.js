import React, { Component } from 'react';
import styled from 'styled-components';

const AppWrapper = styled.main`
  color: red
`;

class App extends Component {
  render() {
    return (
      <AppWrapper className="app">
        React App
      </AppWrapper>
    );
  }
}

export default App;
