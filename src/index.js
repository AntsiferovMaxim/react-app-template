import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal } from 'styled-components'
import { normalize } from 'styled-normalize'

import App from './features/app';
import registerServiceWorker from './registerServiceWorker';
import {globalStyles} from 'ui/theme';

injectGlobal`${normalize} ${globalStyles}`;

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
