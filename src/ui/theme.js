const css = String.raw;

export const font = {
  main: 'sans-serif'
};

export const globalStyles = css`
  html, body {
    font-size: 16px;
    font-family: ${font.main};
    -webkit-font-smoothing: antialiased;
    margin: 0;
    padding: 0;
  }
  
  a {
    color: inherit;
    text-decoration: none;
    
    &:hover {
      color: inherit;
      text-decoration: none;
    }
  }
  
  * {
    box-sizing: border-box;
  }
  
  h1, h2, h3, h4 {
    margin: 0;
  }
  
  ul, li {
    margin: 0;
    padding: 0;
  }
`;